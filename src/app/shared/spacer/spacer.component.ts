import { Component } from '@angular/core';

@Component({
  selector: 'spacer',
  template: '<ng-content></ng-content>',
  styleUrls: ['./spacer.component.scss'],
})
export class SpacerComponent {}
