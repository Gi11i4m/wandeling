import { LocationHintBuilder as LHB } from './location-hint';

export default [
  new LHB()
    .withHint(
      'Een kaarsje aansteken, een schoentje passen, een kleedje shoppen, een koffietje proeven of een tattoo kiezen… dat kan HIER allemaal!'
    )
    .withImages(['monade.jpg'])
    .withLocation([51.10176, 5.147438])
    .build(),
  new LHB()
    .withHint('Ontmoet hier de schattigste, wolligste mascottes van ons dorp.')
    .withImages(['alpaca.jpg'])
    .withLocation([51.105069, 5.139131])
    .build(),
  new LHB().withHint('Volg Trixie!').withImages(['trixie.jpg']).build(),
  new LHB()
    .withHint(
      'Dankzij deze nutteloze constructie moet ik in Gerhoeven niet te hard over-drijven.'
    )
    .withLocation([51.10852, 5.137635])
    .build(),
  new LHB()
    .withHint('Het paard zonder sinterklaas.')
    .withImages(['manege.jpg'])
    .withLocation([51.107973, 5.153353])
    .build(),
  new LHB().withImages(['manege_splitsing.jpg']).build(),
  new LHB()
    .withHint('Is het nu omhoog of omlaag?')
    .withImages(['omhoog_omlaag.jpg'])
    .withLocation([51.109922, 5.157527])
    .build(),
  new LHB()
    .withHint('(blijven volgen tot de volgende straat)')
    .withImages(['konijnenpad.jpg'])
    .build(),
  new LHB()
    .withHint('Run Forest, run!')
    .withImages(['forest.png'])
    .withLocation([51.107678, 5.161752])
    .build(),
  new LHB()
    .withHint('Into the woods...')
    .withImages(['bospad_onze_hof.jpg'])
    .withLocation([51.106425, 5.159615])
    .build(),
  new LHB()
    .withImages(['terras.jpg'])
    .withLocation([51.104304, 5.16178])
    .build(),
  new LHB()
    .withHint(
      'Ik heb dringend een knipbeurt nodig. Ga eens kijken of de kapster open is!'
    )
    .withImages(['kapper.jpg'])
    .withLocation([51.102107, 5.160916])
    .build(),
  new LHB()
    .withHint(
      'Mooie huizen in het Meulenven. Hoe zouden ze er aan de achterkant uitzien?'
    )
    .withImages(['bos.jpg'])
    .withLocation([51.101345, 5.161304])
    .build(),
  new LHB()
    .withImages(['meulenven_achterkant.jpg'])
    .withLocation([51.1009, 5.156837])
    .build(),
  new LHB()
    .withImages(['elektriciteit.jpg'])
    .withLocation([51.101297, 5.150854])
    .build(),
];
